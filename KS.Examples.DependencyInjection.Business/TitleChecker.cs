﻿namespace KS.Examples.DependencyInjection.Business
{
    public class TitleChecker : ITitleChecker
    {
        public bool IsAcceptable(string title)
        {
            return title.ToUpperInvariant() != "test";
        }
    }
}