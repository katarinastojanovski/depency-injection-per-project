﻿namespace KS.Examples.DependencyInjection.Business
{
    public interface ITitleChecker
    {
        bool IsAcceptable(string title);
    }
}