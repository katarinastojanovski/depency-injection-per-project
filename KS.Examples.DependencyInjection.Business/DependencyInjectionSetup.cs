﻿using KS.Examples.DependencyInjection.Business;
using Microsoft.Extensions.DependencyInjection;

// Namespace is intentionally set to .DI so that extension methods are easily discovered.
namespace KS.Examples.DependencyInjection.DI
{
    public static class DependencyInjectionSetup
    {
        public static void AddMyBusinessServices(this IServiceCollection services)
        {
            // Only register implementations from this project.
            services.AddSingleton<ITitleChecker, TitleChecker>();
            services.AddSingleton<IArticlesService, ArticlesService>();
        }
    }
}