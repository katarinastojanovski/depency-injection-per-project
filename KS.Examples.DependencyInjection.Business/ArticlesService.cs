﻿using KS.Examples.DependencyInjection.Data;
using KS.Examples.DependencyInjection.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KS.Examples.DependencyInjection.Business
{
    public class ArticlesService : IArticlesService
    {
        private readonly IArticlesRepository articlesRepository;
        private readonly ITitleChecker titleChecker;

        public ArticlesService(
            IArticlesRepository articlesRepository,
            ITitleChecker titleChecker)
        {
            this.articlesRepository = articlesRepository;
            this.titleChecker = titleChecker;
        }

        public async Task<Article> Add(Article article)
        {
            if (this.titleChecker.IsAcceptable(article.Title))
            {
                return await this.articlesRepository.Add(article).ConfigureAwait(false);
            }

            throw new ArgumentException(nameof(article.Title));
        }

        public async Task<Article> GetById(Guid id)
        {
            return await this.articlesRepository.GetById(id).ConfigureAwait(false);
        }

        public async Task<IEnumerable<Article>> GetAll()
        {
            return await this.articlesRepository.GetAll().ConfigureAwait(false);
        }
    }
}