﻿using Microsoft.Extensions.DependencyInjection;

namespace KS.Examples.DependencyInjection.DI
{
    public static class DependencyInjectionSetup
    {
        // All extension methods that add services for other projects are called here.
        public static void AddMyServices(this IServiceCollection services)
        {
            services.AddMyStaticDataServices();
            services.AddMyBusinessServices();
        }
    }
}