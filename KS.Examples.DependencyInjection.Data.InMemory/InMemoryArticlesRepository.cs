﻿using KS.Examples.DependencyInjection.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KS.Examples.DependencyInjection.Data.InMemory
{
    public class InMemoryArticlesRepository : IArticlesRepository
    {
        private readonly Dictionary<Guid, Article> articles = new Dictionary<Guid, Article>();
        private readonly IRandomizer randomizer;

        public InMemoryArticlesRepository(IRandomizer randomizer)
        {
            this.randomizer = randomizer;
            this.Init();
        }

        public Task<Article> Add(Article article)
        {
            var identifier = Guid.NewGuid();
            article.Id = identifier;
            this.articles.Add(identifier, article);
            return Task.FromResult(article);
        }

        public Task<Article> GetById(Guid id)
        {
            Article article = this.articles.GetValueOrDefault(id);
            return Task.FromResult(article);
        }

        public Task<IEnumerable<Article>> GetAll()
        {
            return Task.FromResult(this.articles.Select(i => i.Value));
        }

        private void Init()
        {
            for (int i = 0; i < 4; i++)
            {
                Article article = new Article()
                {
                    Id = Guid.NewGuid(),
                    Title = this.randomizer.GetString(5),
                };
                this.articles.Add(article.Id, article);
            }
        }
    }
}