﻿namespace KS.Examples.DependencyInjection.Data.InMemory
{
    public interface IRandomizer
    {
        string GetString(int length);
    }
}