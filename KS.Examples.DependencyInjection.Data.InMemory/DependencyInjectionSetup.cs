﻿using KS.Examples.DependencyInjection.Data;
using KS.Examples.DependencyInjection.Data.InMemory;
using Microsoft.Extensions.DependencyInjection;

// Namespace is intentionally set to .DI so that extension methods are easily discovered.
namespace KS.Examples.DependencyInjection.DI
{
    public static class DependencyInjectionSetup
    {
        public static void AddMyStaticDataServices(this IServiceCollection services)
        {
            // Only register implementations from this project.
            services.AddSingleton<IRandomizer, Randomizer>();
            services.AddSingleton<IArticlesRepository, InMemoryArticlesRepository>();
        }
    }
}