﻿using System;

namespace KS.Examples.DependencyInjection.Models
{
    public class Article
    {
        public Guid Id { get; set; }

        public string Title { get; set; }
    }
}