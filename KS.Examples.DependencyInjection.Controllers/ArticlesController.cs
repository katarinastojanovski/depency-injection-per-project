﻿using KS.Examples.DependencyInjection.Business;
using KS.Examples.DependencyInjection.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KS.Examples.DependencyInjection.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ArticlesController : ControllerBase
    {
        private readonly IArticlesService articleService;

        public ArticlesController(IArticlesService articleService)
        {
            this.articleService = articleService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Article>>> GetAll()
        {
            return this.Ok(await this.articleService.GetAll().ConfigureAwait(false));
        }

        [HttpGet("{id}", Name = "Get")]
        public async Task<ActionResult<Article>> Get(Guid id)
        {
            return this.Ok(await this.articleService.GetById(id).ConfigureAwait(false));
        }

        [HttpPost]
        public async Task<ActionResult<Article>> Create([FromBody] Article article)
        {
            var createdArticle = await this.articleService.Add(article).ConfigureAwait(false);
            return this.CreatedAtRoute("Get", new { createdArticle.Id }, createdArticle);
        }
    }
}