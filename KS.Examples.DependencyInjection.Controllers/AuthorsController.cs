﻿using Microsoft.AspNetCore.Mvc;

namespace KS.Examples.DependencyInjection.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class AuthorsController : ControllerBase
    {
    }
}